//Implementando la promesa
function getJSON(url){
  return new Promise(function(resolve, reject){
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url);
    xhr.onreadystatechange = handleResponse;
    xhr.onerror = function(error){reject(error);}; //puede darse un problema de conectividad y        devuelve un error
    xhr.send();

    function handleResponse() {
      if(xhr.readyState === 4) {
        if (xhr.status === 200){
          var employees = JSON.parse(xhr.responseText);
          resolve(employees) //resolve instead of addEmployeesToPage
        }else{
          reject(this.statusText);
        }
      }
    };
  });
}

// var ajaxPromise = getJSON('../data/employees.json') //no se necesita instanciar la promesa
// se incorpora directamente en el ajaxPromise de abajo


//Esta funcion tiene el objeto JSON que retorna el listado de empleados.
function generatListItems(employees)  {
    var statusHTML = '';
    for (var i=0; i<employees.length; i += 1) {
        if (employees[i].inoffice === true) {
            statusHTML += '<li class="in">';
        } else {
            statusHTML += '<li class="out">';
        }
        statusHTML += employees[i].name;
        statusHTML += '</li>';
    }
    
    return statusHTML;
}

//Esta funcion pide los items listados. Retorna el HTML final que se tiene que incorporar al DOM
function generateUnorderedList(listItems) {
    return '<ul class="bulleted">' + listItems +  '</ul>';
}

//Esta funcion incorpora al DOM
function addEmployeesToPage(unorderedList) {
    document.getElementById('employeeList').innerHTML = unorderedList;
}

//Aqui activamos todas las funciones como parte de la promesa
//Ahora podemos usar cualquier numero de funciones para manipular los valores de manera secuencial
getJSON('../data/employees.json').then(generatListItems)
           .then(generateUnorderedList)
           .then(addEmployeesToPage)
           .catch(function(e){
              console.log(e);
           });